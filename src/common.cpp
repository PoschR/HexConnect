//
// Created by robert on 25.02.18.
//

#include "common.h"
#include "Hexagon/Hexagon.h"

#include <SFML/Graphics/RectangleShape.hpp>

#include <imgui.h>

#include <random>



sf::CircleShape make_hexagon_shape(const float HEX_RADIUS, HexagonGrid<UsedData> &hex_grid) {
    sf::CircleShape hexgon(HEX_RADIUS, 6);
    hexgon.setOrigin(HEX_RADIUS, HEX_RADIUS);
    if(hex_grid.layout().format() == HexagonFormat::OddQ || hex_grid.layout().format() == HexagonFormat::EvenQ)
        hexgon.rotate(90.f);
    hexgon.move(HEX_RADIUS, HEX_RADIUS);
    hexgon.setOutlineColor(sf::Color(0, 0, 0));
    hexgon.setOutlineThickness(2.f);

    return hexgon;
}

void center_grid(const int SCREEN_WIDTH, const int SCREEN_HEIGHT, HexagonGrid<UsedData> &hex_grid) { hex_grid.layout().moveTo(ScreenPoint{SCREEN_WIDTH / 2.f - hex_grid.screenWidth() / 2.f, SCREEN_HEIGHT / 2.f - hex_grid.screenHeight() / 2.f}); }


void fill_grid_randomly(HexagonGrid<UsedData> &hex_grid, int min_value, int max_value) {//setup random number geberator


    for(int y = 0; y < hex_grid.height(); y++) {
        for (int x = 0; x < hex_grid.width(); x++) {
            auto& current_hexagon = hex_grid.findByOffset(x,y);

            fill_hexagon_randomly(min_value, max_value, current_hexagon);
        }
    }
}

void draw_line(sf::RenderWindow &window, const sf::Vector2f &first_vector, float thickness,
               const ScreenPoint &next_screen_point);

void fill_hexagon_randomly(int min_value, int max_value, Hexagon<UsedData> &current_hexagon) {
    std::random_device r;
    // Choose a random mean between 1 and 6
    std::default_random_engine e1(r());
    std::uniform_int_distribution<int> uniform_dist(min_value, max_value);

    UsedData data;
    data.color = CellColor(uniform_dist(e1));

    current_hexagon.replaceData(data);
}

void draw_selection_lines(sf::RenderWindow &window, HexagonGrid<UsedData> &hex_grid,
                          AdjacentHexagonSelection<UsedData> &selection)
{
    float thickness = 3.f;
    sf::CircleShape center_dot(thickness);
    center_dot.setOrigin(thickness, thickness);
    center_dot.setFillColor(sf::Color::White);

    for(int i=0; i < selection.currentSelection().size(); i++)
    {
        auto screen_point = selection.currentSelection()[i]->screenCoordinates(hex_grid.layout());
        sf::Vector2f first_vector(screen_point[0], screen_point[1]);

        if(i < selection.size()-1)
        {
            auto next_screen_point = selection.currentSelection()[i+1]->screenCoordinates(hex_grid.layout());
            draw_line(window, first_vector, thickness, next_screen_point);
        }
        /*else (thought it would be a cool idea)
        {
            auto cursor = ImGui::GetMousePos();
            ScreenPoint cursor_point {cursor.x, cursor.y};
            //draw_line(window, first_vector, thickness, cursor_point);
        }*/

        center_dot.setPosition(first_vector);

        window.draw(center_dot);
    }



}

void draw_line(sf::RenderWindow &window, const sf::Vector2f &first_vector, float thickness,
               const ScreenPoint &next_screen_point) {
    sf::Vector2f next_vector(next_screen_point[0], next_screen_point[1]);

    sf::Vector2f size_vector = next_vector - first_vector;
    float length = std::sqrt(size_vector.x * size_vector.x + size_vector.y * size_vector.y);


    sf::RectangleShape line(sf::Vector2f(length, thickness));
    line.setOrigin(0, thickness/2);

    float angle = -std::acos(size_vector.x / length);
    if(next_vector.y > first_vector.y)
                angle = -angle;
    angle = angle / (float) M_PI * 180.f;

    line.setPosition(first_vector);
    line.setRotation(angle);
    line.setFillColor(sf::Color::White);

    window.draw(line);
}


void draw_selected_hexagons(const AdjacentHexagonSelection<UsedData> &selection, sf::RenderWindow &window,
                            sf::CircleShape &hexgon, HexagonGrid<UsedData> &hex_grid) {
    hexgon.setOutlineColor(sf::Color::White);
    for(auto& selected_hexagon : selection.currentSelection()) {
        draw_hexagon(window, hexgon, hex_grid, *selected_hexagon);
    }
}

void draw_normal_hexagons(sf::RenderWindow &window, sf::CircleShape &hexgon, HexagonGrid<UsedData> &hex_grid) {
    hexgon.setOutlineColor(sf::Color::Black);
    for(int y = 0; y < hex_grid.height(); y++) {
        for(int x = 0; x < hex_grid.width(); x++) {
            auto& current_hexagon = hex_grid.findByOffset(x,y);
            if(!current_hexagon.data().selected) {
                draw_hexagon(window, hexgon, hex_grid, current_hexagon);
            }
        }
    }
}

void draw_hexagon(sf::RenderWindow &window, sf::CircleShape &hexgon, HexagonGrid<UsedData> &hex_grid,
                  Hexagon<UsedData> &current_hexagon) {
    auto position = current_hexagon.screenCoordinates(hex_grid.layout());
    auto& color = current_hexagon.data().color;

    switch (color) {
        case CellColor::Empty:
            hexgon.setFillColor(sf::Color::Black);
            break;
        case CellColor::Blue:
            hexgon.setFillColor(sf::Color::Blue);
            break;
        case CellColor::Green:
            hexgon.setFillColor(sf::Color::Green);
            break;
        case CellColor::Yellow:
            hexgon.setFillColor(sf::Color::Yellow);
            break;
        case CellColor::Magenta:
            hexgon.setFillColor(sf::Color::Magenta);
            break;
        default:
            break;
    }

    hexgon.setPosition(position[0], position[1]);

    window.draw(hexgon);
}