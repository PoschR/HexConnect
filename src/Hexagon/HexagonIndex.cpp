//
// Created by robert on 22.02.18.
//

#include "Hexagon/HexagonIndex.h"




HexagonIndex::HexagonIndex(Index axial_q, Index axial_r) :
    axial_q_(axial_q),
    axial_r_(axial_r)
{

}

CubeIndex HexagonIndex::cubeIndex() const{
    return {axial_q_, -axial_q_-axial_r_, axial_r_};
}

OffsetIndex HexagonIndex::offsetIndex(const HexagonFormat &format) const{
    Index column;
    Index row;

    switch (format)
    {
        case HexagonFormat::OddQ:
            column = axial_q_;
            row = ConvertHexAxis(axial_q_, axial_r_, true, true);
            break;

        case HexagonFormat::EvenQ:
            column = axial_q_;
            row = ConvertHexAxis(axial_q_, axial_r_, false, true);
            break;

        case HexagonFormat::OddR:
            column = ConvertHexAxis(axial_r_, axial_q_, true, true);
            row = axial_r_;
            break;

        case HexagonFormat::EvenR:
            column = ConvertHexAxis(axial_r_, axial_q_, false, true);
            row = axial_r_;
            break;
    }

    return {column, row};
}

Index HexagonIndex::q() const{
    return axial_q_;
}

Index HexagonIndex::r() const{
    return axial_r_;
}

bool HexagonIndex::operator==(const HexagonIndex &rhs) const{
    return (axial_q_ == rhs.q()) && (axial_r_ == rhs.r());
}

bool HexagonIndex::operator!=(const HexagonIndex &rhs) const {
    return !((*this) == rhs);
}

Index ConvertHexAxis(Index axis, Index offset_axis, bool odd, bool cube_to_off) {

    if(cube_to_off)
        if(odd)
            return offset_axis + (axis - (axis & 1)) / 2;
        else
            return offset_axis + (axis + (axis & 1)) / 2;
    else
        if(odd)
            return offset_axis - (axis - (axis & 1)) / 2;
        else
            return offset_axis - (axis + (axis & 1)) / 2;
}
