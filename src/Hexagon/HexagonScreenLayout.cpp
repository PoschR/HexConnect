//
// Created by robert on 22.02.18.
//

#include "Hexagon/HexagonScreenLayout.h"


HexagonScreenLayout::HexagonScreenLayout(ScreenPoint position, HexagonRadius pixel_width, const HexagonFormat &format) :
    position_(position),
    radius_(pixel_width),
    format_(format)
{

}

void HexagonScreenLayout::changeFormat(const HexagonFormat &format) {
    format_ = format;
}

HexagonRadius HexagonScreenLayout::radius() const {
    return radius_;
}

HexagonFormat HexagonScreenLayout::format() const {
    return format_;
}

ScreenPoint HexagonScreenLayout::position() const {
    return position_;
}

void HexagonScreenLayout::moveTo(const ScreenPoint &position) {
    position_ = position;
}
