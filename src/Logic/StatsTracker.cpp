//
// Created by robert on 26.02.18.
//

#include "Logic/StatsTracker.h"

StatsTracker::StatsTracker(int initial_score, int delta_score) {

}

void StatsTracker::operator+=(int rhs) {
    delta_score_ = rhs;
    score_ += rhs;
}

void StatsTracker::update() {
    delta_score_ = 0;
}

int StatsTracker::score() const {
    return score_;
}

int StatsTracker::deltaScore() const {
    return delta_score_;
}

int StatsTracker::turns() const {
    return turns_;
}

void StatsTracker::reset() {
    score_ = 0;
    delta_score_ = 0;
    turns_ = 0;
}

void StatsTracker::updateTurn() {
    turns_++;

}
