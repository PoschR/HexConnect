//
// Created by robert on 26.02.18.
//

#include <cassert>
#include "Logic/StateMachine.h"

void StateMachine::transition() {
    assert(current_state != nullptr);

    if(state_change_) {
        current_state->enter();
        state_change_ = false;
    }

    auto* next_state = current_state->transition();
    if(next_state != current_state) {
        state_change_ = true;
        current_state->exit();
        current_state = next_state;
    }
}