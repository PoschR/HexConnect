//
// Created by robert on 28.02.18.
//

#include "Logic/GameStates/InitState.h"
#include "Logic/StateMachine.h"

InitState::InitState(StateMachine &machine, sf::RenderWindow &window, StatsTracker &tracker) :
    window_(window),
    machine_(machine),
    grid_(GRID_WIDTH, GRID_HEIGHT,
        HexagonScreenLayout{
                ScreenPoint{0,0},
                HEX_RADIUS,
                HexagonFormat::OddQ
        }
    ),
    tracker_(tracker)
{

}

IState *InitState::transition() {
    center_grid(window_.getSize().x, window_.getSize().y, grid_);

    auto position = grid_.layout().position();
    grid_.layout().moveTo({position[0] + 100, position[1]-40.f});

    // fill grid randomly
    const int MIN_COLOR = CellColor::Empty+1;
    const int MAX_COLOR =  CellColor::Max-1;
    fill_grid_randomly(grid_, MIN_COLOR, MAX_COLOR);

    // This is used for drawing the grid in the render phase
    hexagon_shape_ = make_hexagon_shape(HEX_RADIUS, grid_);

    tracker_.reset();

    return machine_.state(machine_.interaction_state, grid_, window_, hexagon_shape_, tracker_);
}

void InitState::enter() {

}

void InitState::exit() {

}
