//
// Created by robert on 28.02.18.
//

#include "Logic/GameStates/GameOverState.h"

#include <imgui.h>
#include <Logic/StateMachine.h>
#include <SFML/Window/Keyboard.hpp>

GameOverState::GameOverState(StateMachine &machine, sf::RenderWindow &window) :
    window_(window),
    machine_(machine)
{
    arial_font_.loadFromFile("resources/fonts/arial.ttf");
    game_over_text = sf::Text("Game Over", arial_font_, 60);

    float pos_x = (window_.getSize().x - game_over_text.getLocalBounds().width) / 2.f;
    float pos_y = (window_.getSize().y - game_over_text.getLocalBounds().height) / 2.f - 30;

    game_over_text.setPosition(pos_x, pos_y);

}

IState *GameOverState::transition() {
    window_.clear();
    window_.draw(game_over_text);

    if(ImGui::IsMouseClicked(0))
        return machine_.init_state.get();

    return this;
}

void GameOverState::enter() {

}

void GameOverState::exit() {

}
