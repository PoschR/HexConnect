//
// Created by robert on 25.02.18.
//

#include "Logic/GameStates/GravityState.h"
#include "Logic/GameStates/InteractionState.h"
#include "Logic/StateMachine.h"

#include <imgui.h>

const float FILL_WAITING_TIME = 0.0f;
const float GRAVITY_WAITING_TIME = 0.1f;

GravityState::GravityState(StateMachine &all_states, HexagonGrid<UsedData> &grid, sf::RenderWindow &window,
                           sf::CircleShape &shape) {
    hex_grid_ = &grid;
    toggle_gravity_ = true;
    machine_ = &all_states;
    hex_shape_ = &shape;
    window_ = &window;
    waiting_time_ = GRAVITY_WAITING_TIME;
}

IState * GravityState::transition() {
    if(timer_.getElapsedTime().asSeconds() >= waiting_time_) {
        timer_.restart();

        fillTopRow();
        bool can_apply_gravity = applyGravity();

        if(!can_apply_gravity) {
            return machine_->interaction_state.get();
        }
    }

    window_->clear();

    draw_normal_hexagons(*window_, *hex_shape_, *hex_grid_);

    return this;
}

void GravityState::fillTopRow() const {
    for (int x = 0; x < hex_grid_->width(); x++)
    {
        auto &hexagon = hex_grid_->findByOffset(x, 0);
        if (hexagon.data().color == CellColor::Empty)
            fill_hexagon_randomly(CellColor::Empty+1, CellColor::Max-1, hexagon);
    }
}

bool GravityState::applyGravity() const {
    bool can_apply_gravity = false;//apply gravity

    for (int y = hex_grid_->height() - 1; y > 0; y--) //disregard top row
            {
                for (int x = hex_grid_->width() - 1; x >= 0; x--) {
                    auto &hexagon = hex_grid_->findByOffset(x, y);
                    auto &hex_above = hex_grid_->findByOffset(x, y - 1);

                    if (hexagon.data().color == CellColor::Empty && hex_above.data().color != CellColor::Empty) {
                        hexagon.data().color = hex_above.data().color;
                        hex_above.data().color = CellColor::Empty;
                        can_apply_gravity = true;
                    }
                }
            }

    /*for(int y = 0; y < hex_grid_->height()-1; y++) {
        for(int x = 0; x < hex_grid_->width(); x++) {
            auto &hexagon = hex_grid_->findByOffset(x, y +1);
            auto &hex_above = hex_grid_->findByOffset(x, y );

            if (hexagon.data().color == CellColor::Empty && hex_above.data().color != CellColor::Empty) {
                hexagon.data().color = hex_above.data().color;
                hex_above.data().color = CellColor::Empty;
                can_apply_gravity = true;
            }
        }
    }*/

    return can_apply_gravity;
}


void GravityState::enter() {
    timer_.restart();
    //applyGravity();
}

void GravityState::exit() {

}
