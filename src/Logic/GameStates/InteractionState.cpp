//
// Created by robert on 25.02.18.
//

#include "Logic/GameStates/InteractionState.h"
#include "Logic/GameStates/GravityState.h"
#include "Logic/StateMachine.h"

#include <imgui.h>

InteractionState::InteractionState(StateMachine &machine, HexagonGrid<UsedData> &grid, sf::RenderWindow &renderWindow,
                                   sf::CircleShape &shape, StatsTracker &tracker) :
    selection_(
            [](Hexagon<UsedData>& hexagon, bool selection)
            {
                hexagon.data().selected = selection;
            }
    )
{
    machine_ =  &machine;
    hex_grid_ = &grid;
    hex_shape_ = &shape;
    window_ = &renderWindow;
    tracker_ = &tracker;
}


IState * InteractionState::transition() {
    ImVec2 cursor_pos = ImGui::GetMousePos();
    auto& hex = hex_grid_->findByScreenCoordinates(cursor_pos.x, cursor_pos.y);


    if (ImGui::IsMouseDown(0) && CellColor::Empty != hex.data().color)
    {
        selection_.add(hex);
    }
    else if(ImGui::IsMouseReleased(0))
    {
        auto selected_hexagons = selection_.clear();
        for (auto &selected_hexagon : selected_hexagons)
        {
            if (selected_hexagons.size() >= 3)
                selected_hexagon->data().color = CellColor::Empty;
        }
        if(selected_hexagons.size() >= 3)
        {
            (*tracker_) += selected_hexagons.size();
            return machine_->state(machine_->gravity_state, *hex_grid_, *window_, *hex_shape_);
        }
    }

    if(!selection_.currentSelection().empty() && ImGui::IsMouseClicked(1)) {
        selection_.clear();
    }

    //get rid of all the old stuff
    window_->clear();

    //draw the normal hexagons
    draw_normal_hexagons(*window_, *hex_shape_, *hex_grid_);

    draw_selected_hexagons(selection_, *window_, *hex_shape_, *hex_grid_);

    // draw lines
    draw_selection_lines(*window_, *hex_grid_, selection_);

    if(tracker_->turns() > MAX_TURNS) {
        return machine_->state(machine_->game_over_state, *window_);
    }

    return this;
}

HexagonGrid<UsedData> &InteractionState::grid() {
    return *hex_grid_;
}

const HexagonGrid<UsedData> &InteractionState::grid() const {
    return *hex_grid_;
}

void InteractionState::enter() {
    tracker_->updateTurn();
}

void InteractionState::exit() {

}
