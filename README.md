# HexConnect
Connect at least three hexagons of the same color in order to release them from the confinement of a tightly
packed grid.

The project requires [ImGui](https://github.com/ocornut/imgui) and [ImGui-SFML](https://github.com/eliasdaler/imgui-sfml) to work. Place them in the `3rdParty` folder and use the custom CMakeLists.txt for
imgui-sfml. 

## TODO
 - Score
 - High Score List
 - Combos

![alt text](https://gitlab.com/PoschR/HexConnect/raw/463e8ffb083ea5f1189519576b61f23009adc946/gameplay_hexconnect.gif)

The game was inspired by a code challenge I did, where I originally used the Unreal Engine 4. I wondered how much less overhead
the project had when starting from scratch.
