#include <imgui.h>
#include <imgui-SFML.h>

#include <vector>


#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/Text.hpp>

#include "Hexagon/HexagonGrid.h"
#include "Logic/StateMachine.h"
#include "Logic/StatsTracker.h"

#include "common.h"

void ProcessEvents(sf::RenderWindow &window);


int main() {
    const int SCREEN_WIDTH = 1024;
    const int SCREEN_HEIGHT = 768;

    sf::Font arial;
    if(!arial.loadFromFile("resources/fonts/arial.ttf"))
        throw std::runtime_error("main(): arial.ttf not found!");

    sf::Text score_text{"Score: ", arial };
    score_text.setPosition(60,10);

    sf::Text score_value_text{"0", arial};
    score_value_text.setPosition(score_text.getPosition().x + score_text.getGlobalBounds().width, 11);

    sf::Text turn_text{"Turns left: ", arial};
    turn_text.setPosition(60, 50);

    sf::Text turn_value_text{"0", arial};
    turn_value_text.setPosition(turn_text.getPosition().x + turn_text.getGlobalBounds().width , 51);

    StatsTracker score_tracker;

    //create simple render window
    sf::RenderWindow window(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), "HexConnect", sf::Style::Close|sf::Style::Titlebar);

    //init ImGui to do its thing
    ImGui::SFML::Init(window);

    //use state machine for game states
    StateMachine machine(window, score_tracker);

    //create clock for timing
    sf::Clock frameTime;

    bool update_score = false;
    int score_animation_counter = 0;
    sf::Clock animation_clock;

    //basic window update loop
    while(window.isOpen())
    {
        ProcessEvents(window);

        ImGui::SFML::Update(window, frameTime.restart());

        machine.transition();

        // ----------- update and display score --------------------
        if(score_tracker.deltaScore() > 0) {
            animation_clock.restart();
            update_score = true;
            score_animation_counter = score_tracker.deltaScore();
        }

        const int SCORE_MULTIPLIER = 10;
        if(!update_score) {
            score_value_text.setString(std::to_string(score_tracker.score() * SCORE_MULTIPLIER));
        }
        else if(animation_clock.getElapsedTime().asSeconds() >=  0.05f) {
            animation_clock.restart();
            score_value_text.setString(std::to_string((score_tracker.score() - score_animation_counter) * SCORE_MULTIPLIER));
            score_animation_counter--;
        }

        if(score_animation_counter <= 0)  {
            update_score = false;
        }

        turn_value_text.setString(std::to_string(MAX_TURNS - score_tracker.turns() +1));

        score_tracker.update();


        window.draw(score_text);
        window.draw(score_value_text);
        window.draw(turn_text);
        window.draw(turn_value_text);

        ImGui::SFML::Render(window);
        window.display();
    }

    return 0;
}




void ProcessEvents(sf::RenderWindow &window) {
    sf::Event event;
    //empty event queue
    while(window.pollEvent(event)) {
        //delegate event to ImGui
        ImGui::SFML::ProcessEvent(event);

        //close window if users closes it
        if (event.type == sf::Event::Closed) {
            window.close();
        }
    }
}