cmake_minimum_required(VERSION 3.9)
project(HexConnect)

set(CMAKE_CXX_STANDARD 11)

#include all of the custom CMake findings
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake")

#include the basic include directory
include_directories(include)

#find and include sfml
find_package(SFML COMPONENTS graphics window system REQUIRED)
include_directories(${SFML_INCLUDE_DIRS})

#include 3rd party libraries
add_subdirectory(3rdParty/imgui-sfml)

#include ImGui with SFML support
include_directories(${IMGUI_SFML_INCLUDE_DIRS})

#find and include OpenGL
find_package(OpenGL REQUIRED)
include_directories(${OPEN_GL_INCLUDE_DIR})

add_executable(HexConnect main.cpp ${IMGUI_SOURCES} ${IMGUI_SFML_SOURCES} include/Hexagon/Hexagon.inl include/Hexagon/Hexagon.h include/Hexagon/HexagonGrid.inl include/Hexagon/HexagonGrid.h src/Hexagon/HexagonIndex.cpp include/Hexagon/HexagonIndex.h src/Hexagon/HexagonScreenLayout.cpp include/Hexagon/HexagonScreenLayout.h include/Hexagon/AdjacentHexagonSelection.inl include/Hexagon/AdjacentHexagonSelection.h include/Logic/State.h src/Logic/GameStates/InteractionState.cpp include/Logic/GameStates/InteractionState.h src/Logic/GameStates/GravityState.cpp include/Logic/GameStates/GravityState.h include/common.h src/common.cpp include/Logic/StateMachine.h src/Logic/StateMachine.cpp src/Logic/StatsTracker.cpp include/Logic/StatsTracker.h src/Logic/GameStates/GameOverState.cpp include/Logic/GameStates/GameOverState.h src/Logic/GameStates/InitState.cpp include/Logic/GameStates/InitState.h)

target_link_libraries(HexConnect ${SFML_LIBRARIES} ${OPENGL_LIBRARIES})