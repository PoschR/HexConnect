//
// Created by robert on 23.02.18.
//

#ifndef HEXCONNECT_HEXAGONSELECTION_H
#define HEXCONNECT_HEXAGONSELECTION_H

#include "Hexagon.h"
#include "HexagonGrid.h"

#include <vector>
#include <functional>
#include <algorithm>

template<typename T>
using Selection = std::vector<Hexagon<T>*>;

template<typename T>
using SelectionFunction = std::function<void(Hexagon<T>& selected_hexagon, bool added)>;

/**
 * Handles a selection of hexagons that are adjacent to each other and are of the same type.
 * For this, the stored data class has to provide the == and != operator.
 * @tparam T
 */
template <typename T>
class AdjacentHexagonSelection {
public:

    explicit AdjacentHexagonSelection(SelectionFunction<T>&& on_selection = []{});

    void add(Hexagon<T>& hexagon);

    Selection<T> clear();

    const Selection<T>& currentSelection() const;

    Size size();

private:
    Selection<T> selection_;
    SelectionFunction<T> on_selection_;
};

#include "AdjacentHexagonSelection.inl"

#endif //HEXCONNECT_HEXAGONSELECTION_H
