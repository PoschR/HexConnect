//
// Created by robert on 22.02.18.
//

#ifndef HEXCONNECT_HEXAGONINDEX_H
#define HEXCONNECT_HEXAGONINDEX_H

#include <array>

using Index = int;
using OffsetIndex = std::array<Index, 2>;
using CubeIndex = std::array<Index,3>;

enum struct HexagonFormat {
    OddQ,
    EvenQ,
    OddR,
    EvenR
};


/**
 * Handle index related stuff here
 */
class HexagonIndex {
public:
    /**
     * Generate hexagon with axial coordinates and given format.
     * @param axial_q Horizontal position in axial coordinates
     * @param axial_r Vertical position in axial coordinates
     */
    HexagonIndex(Index axial_q, Index axial_r);

    /**
     * Compute the cube indices x,y,z and return them
     * @return Cube indices in array of format {x,y,z}
     */
    CubeIndex cubeIndex() const;

    /**
     * Compute offset indices column, row and return them
     * @return Offset coordinates in array of format {column, row}
     */
    OffsetIndex offsetIndex(const HexagonFormat &format) const;

    /**
     * Return horizontal axial coordinate
     * @return
     */
    Index q() const;

    /**
     * Return vertical axial coordinate
     * @return
     */
    Index r() const;


    bool operator==(const HexagonIndex& rhs) const;

    bool operator!=(const HexagonIndex& rhs) const;

private:
    Index axial_q_;
    Index axial_r_;

};

//Helper function for converting indices
Index ConvertHexAxis(Index axis, Index offset_axis, bool odd, bool cube_to_off=true);


#endif //HEXCONNECT_HEXAGONINDEX_H
