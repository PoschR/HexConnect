//
// Created by robert on 21.02.18.
//

//inclue only for CLion!
//#include "Hexagon.h"

template<typename T>
Hexagon<T>::Hexagon(const HexagonIndex& index, const T& data) :
        index_(index),
        data_(data)
{

}

template<typename T>
Hexagon<T> Hexagon<T>::FromOffset(Index column, Index row, const T &data, const HexagonFormat &format) {

    Index axial_q;
    Index axial_r;

    switch (format)
    {
        case HexagonFormat::OddQ:
            axial_q = column;
            axial_r = ConvertHexAxis(column, row, true, false);
            break;

        case HexagonFormat::EvenQ:
            axial_q = column;
            axial_r = ConvertHexAxis(column, row, false, false);
            break;

        case HexagonFormat::OddR:
            axial_q = ConvertHexAxis(row, column, true, false);
            axial_r = row;
            break;

        case HexagonFormat::EvenR:
            axial_q = ConvertHexAxis(row, column, false, false);
            axial_r = row;
            break;
    }


    return Hexagon{HexagonIndex(axial_q, axial_r), data};
}

template<typename T>
bool Hexagon<T>::adjacentTo(const Hexagon &other_hexagon) const{
    return distanceTo(other_hexagon) == 1;
}

template<typename T>
Distance Hexagon<T>::distanceTo(const Hexagon &other_hexagon) const{
    CubeIndex this_index = index_.cubeIndex();
    CubeIndex other_index = other_hexagon.index().cubeIndex();

    return (std::abs(this_index[0] - other_index[0])
           + std::abs(this_index[1] - other_index[1])
           + std::abs(this_index[2] - other_index[2])) / 2;
}

template<typename T>
ScreenPoint Hexagon<T>::screenCoordinates(const HexagonScreenLayout& layout) const
{
    ScreenPoint point;

    //compute screen space according to formulas
    if(layout.format() == HexagonFormat::OddQ || layout.format() == HexagonFormat::EvenQ)
    {
        point[0] = layout.radius() * 3 / 2 * index().q() + layout.position()[0] + layout.radius();
        point[1] = layout.radius() * SQRT_3F * (index().r() + index().q()/2.f)+ layout.position()[1] + layout.radius();
    }
    else
    {
        point[0] = layout.radius() * SQRT_3F * (index().q() + index().r() / 2.f) + layout.position()[0] + layout.radius();
        point[1] = layout.radius() * 3 / 2 * index().r() + layout.position()[1] + layout.radius();
    }

    return point;
}

template<typename T>
const HexagonIndex &Hexagon<T>::index() const {
    return index_;
}

template<typename T>
const T& Hexagon<T>::data() const{
    return  data_;
}

template<typename T>
T& Hexagon<T>::data(){
    return  data_;
}



template<typename T>
void Hexagon<T>::replaceData(const T& data)  {
    data_ = data;
}

template<typename T>
bool Hexagon<T>::operator==(const Hexagon& rhs) const {
    return this->index() == rhs.index();
}

template<typename T>
bool Hexagon<T>::operator!=(const Hexagon& rhs) const {
    return this->index() != rhs.index();
}
