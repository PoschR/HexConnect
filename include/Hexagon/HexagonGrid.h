//
// Created by robert on 21.02.18.
//

#ifndef HEXCONNECT_HEXAGONGRID_H
#define HEXCONNECT_HEXAGONGRID_H

#include "Hexagon.h"
#include "HexagonScreenLayout.h"

#include <vector>

using Size = unsigned int;
using ScreenSize = float;

/**
 * Stores hexagons and handles screen-to-index conversions
 * @tparam T Data which the hexagons store
 */
template <typename T>
class HexagonGrid {
public:

    /**
     * Construct a hexagon grid with the given format
     * @param width Width of hexagon grid
     * @param height Height of hexagon grid
     * @param format Flat or Pointed grid, even or odd
     * @param pixel_width The resolution of one hexagon radius in the grid
     * @param fill_data default value of the grid
     */
    HexagonGrid(Size width, Size height, const HexagonScreenLayout& layout, const T &fill_data = T());

    /**
     * Find hexagon by giving a index. Returns out-of-bounds hexagon, if index isn't valid.
     * @param index Index with axial coordiantes
     * @return Hexagon at given index
     */
    const Hexagon<T>& findByIndex(const HexagonIndex& index) const;


    Hexagon<T>& findByIndex(const HexagonIndex& index);


    /**
     * Find hexagon by giving offset coordinates. Returns out-of-bounds hexagon, if index isn't valid.
     * @param column Horizontal coordinate of offset coordinate
     * @param row Vertical coordinate of offset coordinate
     * @return
     */
    const Hexagon<T>& findByOffset(Index column, Index row) const;

    Hexagon<T>& findByOffset(Index column, Index row);


    /**
     * Find a hexagon on the grid by providing screen coordiantes. Returns out-of-bounds hexagon if it fails.
     * @param x x coordinate of pixel
     * @param y y coordinate of pixel
     * @return Hexagon at the
     */
    const Hexagon<T> &findByScreenCoordinates(ScreenCoordinate x, ScreenCoordinate y) const;

    Hexagon<T> &findByScreenCoordinates(ScreenCoordinate x, ScreenCoordinate y);

    /**
     * Get the out-of-bounds hexagon
     * @return Hexagon that is representative of out of bounds
     */
    const Hexagon<T>& outOfBounds() const;

    Hexagon<T>& outOfBounds();


    // getters

    const HexagonScreenLayout& layout() const;

    HexagonScreenLayout& layout();


    Size width() const;

    Size height() const;


    //TODO fix screen conversions
    ScreenSize screenWidth() const;

    ScreenSize screenHeight() const;


private:
    Size width_;
    Size height_;
    HexagonScreenLayout layout_;
    std::vector<Hexagon<T>> hexagon_grid_;

    bool offsetInBounds(Index column, Index row) const;

    Size index1DByOffset(Index column, Index row) const;

    HexagonIndex hexRound(float q, float r) const;

    ScreenSize ScreenDiffFullRadius(ScreenCoordinate begin_coord, ScreenCoordinate end_coord) const;

    ScreenSize ScreenDiffHeightFromRadius(ScreenCoordinate begin_coord, ScreenCoordinate end_coord) const;

};

#include "HexagonGrid.inl"
#endif //HEXCONNECT_HEXAGONGRID_H
