//
// Created by robert on 21.02.18.
//

#ifndef HEXCONNECT_HEXAGON_H
#define HEXCONNECT_HEXAGON_H

#include "HexagonIndex.h"
#include "HexagonScreenLayout.h"

using Distance = int;

//Precompute the square root of 3
const float SQRT_3F = 1.73205080757f;

/**
 * This represents a hexagon, which can hold data
 */

template<typename T>
class Hexagon {
public:
    /**
     * Generate hexagon with axial coordinates and given format.
     * @param axial_q Horizontal position in axial coordinates
     * @param axial_r Vertical position in axial coordinates
     * @param format Which format is used for the hexagon
     */
    Hexagon(const HexagonIndex& index, const T& data);

    /**
     * Generate a HexagonIndex by using normal offset coordinates
     * @param column x position
     * @param row y position
     * @param format used format
     * @return HexagonIndex with converted indices
     */
    static Hexagon FromOffset(Index column, Index row, const T &data, const HexagonFormat &format);

    /**
     * Check whether given hexagon is adjacent to this hexagon
     * @param other_hexagon
     * @return true if adjacent, false otherwise
     */
    bool adjacentTo(const Hexagon& other_hexagon) const;

    /**
     * Computes distance to other hexagon
     * @param other_hexagon
     * @return Distance to the other hexagon
     */
    Distance distanceTo(const Hexagon& other_hexagon) const;


    /**
     * Return the screen coordinates, given the layout
     * @param layout
     * @return
     */
    ScreenPoint screenCoordinates(const HexagonScreenLayout& layout) const;


    /**
     * Return the index of the hexagon
     * @return
     */
    const HexagonIndex& index() const;

    /**
     * Return thte data attached to the cell
     * @return
     */
    const T& data() const;

    T& data();

    /**
     * Replace current data with new value.
     * @param data
     */
    void replaceData(const T& data);


    bool operator==(const Hexagon& rhs) const;

    bool operator!=(const Hexagon& rhs) const;

private:
    HexagonIndex index_;
    T data_;
};


#include "Hexagon.inl"

#endif //HEXCONNECT_HEXAGON_H
