//
// Created by robert on 22.02.18.
//

#ifndef HEXCONNECT_HEXAGONSCREENLAYOUT_H
#define HEXCONNECT_HEXAGONSCREENLAYOUT_H

#include "HexagonIndex.h"

using HexagonRadius = float;
using ScreenCoordinate = float;
using ScreenPoint = std::array<ScreenCoordinate, 2>;

class HexagonScreenLayout {
public:

    HexagonScreenLayout(ScreenPoint position, HexagonRadius pixel_width, const HexagonFormat &format);


    void changeFormat(const HexagonFormat& format);

    HexagonRadius radius() const;

    HexagonFormat format() const;

    ScreenPoint position() const;

    void moveTo(const ScreenPoint&  position);

private:
    ScreenPoint position_;
    HexagonRadius radius_;
    HexagonFormat format_;

};


#endif //HEXCONNECT_HEXAGONSCREENLAYOUT_H
