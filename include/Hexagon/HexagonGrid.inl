//
// Created by robert on 21.02.18.
//

//include only for CLion Syntax highlighting!
//#include "HexagonGrid.h"

#include <cmath>
#include "HexagonGrid.h"

template<typename T>
HexagonGrid<T>::HexagonGrid(Size width, Size height, const HexagonScreenLayout& layout, const T &fill_data)
        :
    width_(width),
    height_(height),
    layout_(layout)
{
    for(int y=0; y < height_; y++)
    {
        for(int x =0; x < width_; x++)
        {
            hexagon_grid_.push_back(Hexagon<T>::FromOffset(x, y, fill_data, layout_.format()));
        }
    }

    //use last element as out of bounds element
    hexagon_grid_.push_back(Hexagon<T>::FromOffset(0, height_, T(), layout_.format()));
}

template<typename T>
const Hexagon<T>& HexagonGrid<T>::findByIndex(const HexagonIndex& index) const {
    //use offset index
    OffsetIndex offset_index = index.offsetIndex(layout_.format());

    return findByOffset(offset_index[0], offset_index[1]);
}

template<typename T>
Hexagon<T>& HexagonGrid<T>::findByIndex(const HexagonIndex& index) {
    //use offset index
    OffsetIndex offset_index = index.offsetIndex(layout_.format());

    return findByOffset(offset_index[0], offset_index[1]);
}

template<typename T>
const Hexagon<T>& HexagonGrid<T>::findByOffset(Index column, Index row) const{
    //check whether index is valid, if invalid return the out-of-bounds hexagon
    if(offsetInBounds(column, row))
        return outOfBounds();

    return hexagon_grid_[index1DByOffset(column, row)];
}
template<typename T>
Hexagon<T>& HexagonGrid<T>::findByOffset(Index column, Index row){
    //check whether index is valid, if invalid return the out-of-bounds hexagon
    if(offsetInBounds(column, row))
        return outOfBounds();

    return hexagon_grid_[index1DByOffset(column, row)];
}


template<typename T>
const Hexagon<T> & HexagonGrid<T>::findByScreenCoordinates(ScreenCoordinate x, ScreenCoordinate y) const
{
    float sub_index_q;
    float sub_index_r;
    ScreenPoint position = layout().position();

    float relative_x = x - position[0] - layout().radius();
    float relative_y = y - position[1] - layout().radius();

    //compute according to formula
    if(layout_.format() == HexagonFormat::OddQ || layout_.format() == HexagonFormat::EvenQ)
    {
        sub_index_q = relative_x * 2.f / 3.f / layout_.radius();
        sub_index_r = (-relative_x/3.f + SQRT_3F / 3.f * relative_y) / layout_.radius();
    }
    else
    {
        sub_index_q = (relative_x * SQRT_3F / 3.f - relative_y / 3.f) / layout_.radius();
        sub_index_r = relative_y * 2.f / 3.f / layout_.radius();
    }

    return findByIndex(hexRound(sub_index_q, sub_index_r));
}

template<typename T>
Hexagon<T> & HexagonGrid<T>::findByScreenCoordinates(ScreenCoordinate x, ScreenCoordinate y)
{
    float sub_index_q;
    float sub_index_r;
    ScreenPoint position = layout().position();

    float relative_x = x - position[0] - layout().radius();
    float relative_y = y - position[1] - layout().radius();

    //compute according to formula
    if(layout_.format() == HexagonFormat::OddQ || layout_.format() == HexagonFormat::EvenQ)
    {
        sub_index_q = relative_x * 2.f / 3.f / layout_.radius();
        sub_index_r = (-relative_x/3.f + SQRT_3F / 3.f * relative_y) / layout_.radius();
    }
    else
    {
        sub_index_q = (relative_x * SQRT_3F / 3.f - relative_y / 3.f) / layout_.radius();
        sub_index_r = relative_y * 2.f / 3.f / layout_.radius();
    }

    return findByIndex(hexRound(sub_index_q, sub_index_r));
}

template<typename T>
const Hexagon<T>& HexagonGrid<T>::outOfBounds() const{
    return hexagon_grid_.back();
}
template<typename T>
Hexagon<T>& HexagonGrid<T>::outOfBounds(){
    return hexagon_grid_.back();
}


template<typename T>
Size HexagonGrid<T>::index1DByOffset(Index column, Index row) const { return row * width_ + column; }

template<typename T>
bool HexagonGrid<T>::offsetInBounds(Index column, Index row) const {
    return column < 0 || row < 0 || row >= height_ || column >= width_;
}

template<typename T>
HexagonIndex HexagonGrid<T>::hexRound(float q, float r) const {
    float x = q;
    float z = r;
    float y = -x - z;

    float rx = std::round(x);
    float ry = std::round(y);
    float rz = std::round(z);

    float x_diff = std::abs(x - rx);
    float y_diff = std::abs(y - ry);
    float z_diff = std::abs(z - rz);

    if (x_diff > y_diff && x_diff > z_diff)
        rx = -ry - rz;
    else if (y_diff > z_diff)
        ry = -rx - rz;
    else
        rz = -rx - ry;

    return HexagonIndex((Index) rx, (Index) rz);
}

template<typename T>
const HexagonScreenLayout& HexagonGrid<T>::layout() const
{
    return layout_;
}

template<typename T>
HexagonScreenLayout& HexagonGrid<T>::layout()
{
    return layout_;
}

template<typename T>
Size HexagonGrid<T>::width() const
{
    return width_;
}

template<typename T>
Size HexagonGrid<T>::height() const
{
    return height_;
}

template<typename T>
ScreenSize HexagonGrid<T>::ScreenDiffFullRadius(ScreenCoordinate begin_coord, ScreenCoordinate end_coord) const{
    return end_coord - begin_coord + 2 * layout().radius();
}

template<typename T>
ScreenSize HexagonGrid<T>::ScreenDiffHeightFromRadius(ScreenCoordinate begin_coord, ScreenCoordinate end_coord) const{
    return end_coord - begin_coord + SQRT_3F * layout().radius() * 3.f / 2.f ;
}

template<typename T>
ScreenSize HexagonGrid<T>::screenWidth() const {
    auto& first_hexagon = findByOffset(0,0);
    auto& far_right_hexagon = findByOffset(width()-1,0);

    ScreenCoordinate first_x = first_hexagon.screenCoordinates(layout())[0];
    ScreenCoordinate far_right_x = far_right_hexagon.screenCoordinates(layout())[0];

    if(layout().format() == HexagonFormat::EvenQ || layout().format() == HexagonFormat::OddQ)
        return ScreenDiffFullRadius(first_x, far_right_x);
    else
        return ScreenDiffHeightFromRadius(first_x, far_right_x);
}

template<typename T>
ScreenSize HexagonGrid<T>::screenHeight() const {
    auto& first_hexagon = findByOffset(0,0);
    auto& bottom_hexagon = findByOffset(0, height()-1);

    ScreenCoordinate first_y = first_hexagon.screenCoordinates(layout())[1];
    ScreenCoordinate bottom_y = bottom_hexagon.screenCoordinates(layout())[1];

    if(layout().format() == HexagonFormat::EvenQ || layout().format() == HexagonFormat::OddQ)
        return ScreenDiffHeightFromRadius(first_y, bottom_y);
    else
        return ScreenDiffFullRadius(first_y, bottom_y);
}

