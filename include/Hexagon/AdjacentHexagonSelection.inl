//
// Created by robert on 23.02.18.
//


template<typename T>
AdjacentHexagonSelection<T>::AdjacentHexagonSelection(SelectionFunction<T>&& on_selection) :
    on_selection_(std::move(on_selection))
{

}

template<typename T>
void AdjacentHexagonSelection<T>::add(Hexagon<T>& hexagon){

    if(selection_.size() >= 1) {
        if(selection_.size() > 1) {
            auto* second_last =  selection_[selection_.size()-2];
            if(second_last == &hexagon) {
                on_selection_(*selection_[selection_.size()-1], false);
                selection_.pop_back();
                return;
            }
        }
        if(std::find(selection_.begin(), selection_.end(), &hexagon) != selection_.end())
            return;
        else if(!selection_.back()->adjacentTo(hexagon))
            return;
        else if(hexagon.data() != selection_[0]->data())
            return;
    }

    selection_.push_back(&hexagon);
    on_selection_(hexagon, true);

}

template<typename T>
Selection<T> AdjacentHexagonSelection<T>::clear() {
    for(auto& hexagon: selection_)
        on_selection_(*hexagon, false);

    return std::move(selection_);
}

template<typename T>
const Selection<T>& AdjacentHexagonSelection<T>::currentSelection() const {
    return selection_;
}

template<typename T>
Size AdjacentHexagonSelection<T>::size() {
    return selection_.size();
}