//
// Created by robert on 25.02.18.
//

#ifndef HEXCONNECT_COMMON_H
#define HEXCONNECT_COMMON_H

#include <memory>

#include "Hexagon/Hexagon.h"
#include "Hexagon/HexagonGrid.h"
#include "Hexagon/AdjacentHexagonSelection.h"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/CircleShape.hpp>

enum CellColor {
    Empty = 0,
    Blue = 1,
    Yellow = 2,
    Green = 3,
    Magenta = 4,
    Max
};

struct CellData {
    CellColor color = CellColor::Empty;
    bool selected = false;

    //required for selection to work
    inline bool operator==(CellData& rhs) {
        return color ==  rhs.color;
    }

    inline bool operator!=(CellData& rhs) {
        return !(*this == rhs);
    }
};

using UsedData = CellData;

const unsigned int MAX_TURNS = 5;

template<typename T, typename ...Args>
std::unique_ptr<T> make_unique( Args&& ...args ) noexcept
{
    return std::unique_ptr<T>( new T( std::forward<Args>(args)... ) );
}

void fill_grid_randomly(HexagonGrid<UsedData> &hex_grid, int min_value, int max_value);

void center_grid(const int SCREEN_WIDTH, const int SCREEN_HEIGHT, HexagonGrid<UsedData> &hex_grid);

sf::CircleShape make_hexagon_shape(const float HEX_RADIUS, HexagonGrid<UsedData> &hex_grid);


void fill_hexagon_randomly(int min_value, int max_value, Hexagon<UsedData> &current_hexagon);

void draw_hexagon(sf::RenderWindow &window, sf::CircleShape &hexgon, HexagonGrid<UsedData> &hex_grid,
                  Hexagon<UsedData> &current_hexagon);


void draw_normal_hexagons(sf::RenderWindow &window, sf::CircleShape &hexgon, HexagonGrid<UsedData> &hex_grid);

void draw_selected_hexagons(const AdjacentHexagonSelection<UsedData> &selection, sf::RenderWindow &window,
                            sf::CircleShape &hexgon, HexagonGrid<UsedData> &hex_grid);


void draw_selection_lines(sf::RenderWindow &window, HexagonGrid<UsedData> &hex_grid,
                          AdjacentHexagonSelection<UsedData> &selection);

#endif //HEXCONNECT_COMMON_H
