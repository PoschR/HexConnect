//
// Created by robert on 25.02.18.
//

#ifndef HEXCONNECT_GRAVITYSTATE_H
#define HEXCONNECT_GRAVITYSTATE_H

#include "Logic/State.h"
#include "common.h"
#include "Hexagon/HexagonGrid.h"
#include "Logic/StatsTracker.h"

class InteractionState;
class StateMachine;

/**
 *
 */
class GravityState : public IState {
public:
    GravityState(StateMachine &all_states, HexagonGrid<UsedData> &grid, sf::RenderWindow &window,
                     sf::CircleShape &shape);

    virtual ~GravityState() = default;

    IState* transition() override;

    void enter() override;

    void exit() override;

private:
    bool toggle_gravity_;
    HexagonGrid<UsedData>* hex_grid_;
    StateMachine* machine_;
    sf::RenderWindow* window_;
    sf::CircleShape* hex_shape_;
    sf::Clock timer_;
    float waiting_time_;

    bool applyGravity() const;

    void fillTopRow() const;
};


#endif //HEXCONNECT_GRAVITYSTATE_H
