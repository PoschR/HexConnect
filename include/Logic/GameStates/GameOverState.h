//
// Created by robert on 28.02.18.
//

#ifndef HEXCONNECT_GAMEOVERSTATE_H
#define HEXCONNECT_GAMEOVERSTATE_H

#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Text.hpp>
#include "Logic/State.h"

class StateMachine;

#include "common.h"

class GameOverState : public IState {
public:
    explicit GameOverState(StateMachine &machine, sf::RenderWindow &window);


    IState *transition() override;

    void enter() override;

    void exit() override;

private:
    StateMachine& machine_;
    sf::RenderWindow& window_;
    sf::Font arial_font_;
    sf::Text game_over_text;

};


#endif //HEXCONNECT_GAMEOVERSTATE_H
