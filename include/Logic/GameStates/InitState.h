//
// Created by robert on 28.02.18.
//

#ifndef HEXCONNECT_INITSTATE_H
#define HEXCONNECT_INITSTATE_H

#include "common.h"

#include "Logic/State.h"
#include "Hexagon/HexagonGrid.h"
#include "Logic/StatsTracker.h"
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/CircleShape.hpp>

class StateMachine;


class InitState : public IState{
public:
    //use hexagon for displaying things
    const float HEX_RADIUS = 40.f;
    const Size GRID_WIDTH = 10;
    const Size GRID_HEIGHT = 11;


    explicit InitState(StateMachine &machine, sf::RenderWindow &window, StatsTracker &tracker);

    IState *transition() override;

    void enter() override;

    void exit() override;

private:
    sf::RenderWindow& window_;
    StatsTracker& tracker_;
    sf::CircleShape hexagon_shape_;
    StateMachine& machine_;
    HexagonGrid<UsedData> grid_;

};


#endif //HEXCONNECT_INITSTATE_H
