//
// Created by robert on 25.02.18.
//

#ifndef HEXCONNECT_INTERACTIONSTATE_H
#define HEXCONNECT_INTERACTIONSTATE_H

#include "Logic/State.h"
#include "Hexagon/AdjacentHexagonSelection.h"
#include "Hexagon/HexagonGrid.h"
#include "Logic/StatsTracker.h"

#include "common.h"

class StateMachine;

/**
 * This is the beginning state of the game. It checks the mouse and adds the hexagon if it's valid.
 */
class InteractionState : public IState {
public:
    InteractionState(StateMachine &machine, HexagonGrid<UsedData> &grid, sf::RenderWindow &renderWindow,
                         sf::CircleShape &shape, StatsTracker &tracker);

    virtual ~InteractionState() = default;


    IState* transition() override;

    void enter() override;

    void exit() override;

    HexagonGrid<UsedData>& grid();

    const HexagonGrid<UsedData>& grid() const;


private:
    AdjacentHexagonSelection<UsedData> selection_;
    HexagonGrid<UsedData>* hex_grid_;
    sf::RenderWindow* window_;
    StateMachine* machine_;
    sf::CircleShape* hex_shape_;
    StatsTracker* tracker_;
};


#endif //HEXCONNECT_INTERACTIONSTATE_H
