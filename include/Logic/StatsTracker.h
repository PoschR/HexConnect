//
// Created by robert on 26.02.18.
//

#ifndef HEXCONNECT_SCORETRACKER_H
#define HEXCONNECT_SCORETRACKER_H


/**
 * Tracks score when it has changed and how much of it.
 */
class StatsTracker {
public:

    explicit StatsTracker(int initial_score=0, int delta_score=0);

    void operator +=(int rhs);

    void update();

    int score() const;

    int deltaScore() const;

    int turns() const;

    void reset();

    void updateTurn();

private:
    int score_  = 0;
    int delta_score_ = 0;

    int turns_ = 0;
};


#endif //HEXCONNECT_SCORETRACKER_H
