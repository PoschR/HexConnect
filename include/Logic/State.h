//
// Created by robert on 25.02.18.
//

#ifndef HEXCONNECT_STATE_H
#define HEXCONNECT_STATE_H

#include <memory>

/**
 * State interface of a state machine. If it transitions to a new state, the new state is created
 * by derived classes.
 */
class IState {
public:
    virtual ~IState() = default;

    /**
     * The transition all derived states have to implement. Returns pointer to the next state.
     * @return Pointer to next state
     */
    virtual IState* transition() = 0;


    virtual void enter() = 0;


    virtual void exit() = 0;
};

#endif //HEXCONNECT_STATE_H
