//
// Created by robert on 25.02.18.
//

#ifndef HEXCONNECT_STATEMACHINE_H
#define HEXCONNECT_STATEMACHINE_H

#include "Logic/GameStates/InitState.h"
#include "Logic/GameStates/InteractionState.h"
#include "Logic/GameStates/GravityState.h"
#include "Logic/GameStates/GameOverState.h"

#include <memory>

//macro for providing all states access
#define DECLARE_STATE(TYPE, VARIABLE_NAME) friend TYPE; std::unique_ptr<TYPE> VARIABLE_NAME; //template<typename ...Args> TYPE* state(Args&& ...args) {if(!VARIABLE_NAME) {VARIABLE_NAME = make_unique<TYPE>(this, std::forward<Args>(args)...);}return VARIABLE_NAME.get();}


/**
 * This state machine handles the allocation and ownership all the states. It forwards the arguments to the initial state;
 */
class StateMachine {

    DECLARE_STATE(InitState, init_state);
    DECLARE_STATE(InteractionState, interaction_state);
    DECLARE_STATE(GravityState, gravity_state);
    DECLARE_STATE(GameOverState, game_over_state);

public:

    template<typename ...Args>
    explicit StateMachine(Args&& ...args) {
        init_state = make_unique<InitState>(*this, std::forward<Args>(args)...);
        current_state = init_state.get();
        state_change_ = true;
    }

    template<typename T, typename ...Args> T* state(std::unique_ptr<T>& state, Args&& ...args)
    {
        if(!state)
        {
            state = make_unique<T>(*this, std::forward<Args>(args)...);
        }
        return state.get();
    }

    void transition();

private:
    IState* current_state = nullptr;
    bool state_change_;
};

#endif //HEXCONNECT_STATEMACHINE_H
